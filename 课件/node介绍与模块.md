# 一  node概要：
    1.1 什么是node
    
    简单的说 Node.js 就是运行在服务端的 JavaScript。

    Node.js 是一个基于Chrome JavaScript 运行时建立的一个平台。

    运行再前端的js的功能作用是什么呢？

    学完node能做什么呢？

    学习成本呢？ 语法与js 无差异，会js呢，并能开发node，无需学习后端的语言如 php java 等

    Node.js是一个事件驱动I/O服务端JavaScript环境，基于Google的V8引擎，V8引擎执行Javascript的速度非常快，性能非常好。

    (所谓事件驱动，简单地说就是你点什么按钮（即产生什么事件），电脑执行什么操作（即调用什么函数）.
    当然事件不仅限于用户的操作. 事件驱动的核心自然是事件)
    有哪些公司在使用呢？

    1.12306 网站，雪球(方三文，雪球创始人及董事长。福建省武平县人。1997年毕业于北京大学中文系) IBM 阿里 paypal..... 
    2.为什么性能优异? 同步异步 node异步处理

    话说有个叫Ryan Dahl的歪果仁，他的工作是用C/C++写高性能Web服务。对于高性能，异步IO、事件驱动是基本原则，
    但是用C/C++写就太痛苦了。于是这位仁兄开始设想用高级语言开发Web服务。他评估了很多种高级语言，发现很多语言
    虽然同时提供了同步IO和异步IO，但是开发人员一旦用了同步IO，他们就再也懒得写异步IO了，所以，最终，Ryan瞄向了
    JavaScript。

    因为JavaScript是单线程执行，根本不能进行同步IO操作，所以，JavaScript的这一“缺陷”导致了它只能使用异步IO。
# 二  环境搭建：
    https://nodejs.org/en/download/ 一路next 直到 finish
    怎么判断安装成功？ node -v
    为什么终端能识别 node 这个命令？
    环境变量：windows 系统的变量

# 三  第一行代码 hello word
    基础：
    console.log("Hello World");

    扩展：
    var http = require('http');

    http.createServer(function (request, response) {

    // 发送 HTTP 头部 
    // HTTP 状态值: 200 : OK
    // 内容类型: text/plain
    response.writeHead(200, {'Content-Type': 'text/plain'});

    // 发送响应数据 "Hello World"
    response.end('Hello World\n');
    }).listen(8888);

    // 终端打印如下信息
    console.log('Server running at http://127.0.0.1:8888/');  
# nvm 介绍与搭建
node verson manage
git  
nvm是node的包管理工具。由于在打开不同的项目时，不同的项目在安装依赖时可能会和node版本有关，
所以这就需要我们在不同的项目下使用不同的node版本。
nvm就是一个比较好用node管理工具，切换node版本。

# nvm安装

https://github.com/coreybutler/nvm-windows/releases



# nvm常用命令

nvm install <version> ## 安装指定版本
nvm uninstall <version> ## 删除已安装的指定版本
nvm use <version> ## 切换使用指定的版本node
nvm ls ## 列出所有安装的版本
nvm ls-remote ## 列出所有远程服务器的版本
nvm current ## 显示当前的版本
nvm alias <name> <version> ## 给不同的版本号添加别名
nvm unalias <name> ## 删除已定义的别名
nvm reinstall-packages <version> ## 在当前版本 node 环境下，重新   全局安装指定版本号的 npm 包
nvm alias default [node版本号] ##设置默认版本



nvm install 

nvm ls

nvm unistall

nvm use 
# 什么是模块


为了编写可维护的代码，我们把很多函数分组，分别放到不同的文件里，这样，每个文件包含的代码就相对较少，很多编程语言都采用这种组织代码的方式。在Node环境中，一个.js文件就称之为一个模块（module）。

# 使用模块有什么好处？

最大的好处是大大提高了代码的可维护性。其次，编写代码不必从零开始。当一个模块编写完毕，就可以被其他地方引用。我们在编写程序的时候，也经常引用其他模块，包括Node内置的模块和来自第三方的模块。

使用模块还可以避免函数名和变量名冲突。相同名字的函数和变量完全可以分别存在不同的模块中，因此，我们自己在编写模块时，不必考虑名字会与其他模块冲突。


什么是commonjs规范:


具体每个子规范的定制进度请查看官方网站的说明：http://commonjs.org/specs/

#CommonJs 对js的定义

**1.模块的引用
2.模块的定义
3.模块的标识** 
  node应用由模块组成，采用的commonjs模块规范。每一个文件就是一个模块，拥有自己独立的作用域，变量，以及方法等，对其他的模块都不可见。CommonJS规范规定，每个模块内部，module变量代表当前模块。这个变量是一个对象，它的exports属性（即module.exports）是对外的接口。加载某个模块，其实是加载该模块的module.exports属性。require方法用于加载模块。
